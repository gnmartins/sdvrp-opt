#include "Solution.h"

#include <iostream>

#include <math.h>
#include <algorithm>
#include <numeric>

Solution::Solution(int routes, int nodes, int capacity, double** edges, std::vector<int>* demands)
{
    this->m_routes = routes;
    this->n_nodes = nodes;
    this->vehicle_capacity = capacity;
    this->costs = edges;
    this->demands = demands;
    this->z = 0;

    std::vector<int> temp(nodes, 0);
    this->current_demand = temp;
    for (int i = 0; i < n_nodes; i++)
        this->current_demand[i] = this->demands->at(i);

}

void Solution::printSolution()
{
    using namespace std;
    cout << "-------------------------------" << endl;
    cout << "SDVRP Solution" << endl;
    cout << "z = " << z << endl;
    for (unsigned int i = 0; i < routes.size(); i++)
    {
        cout << "route " << i << ": ";
        routes[i].printRoute();
    }
    cout << "-------------------------------" << endl;
}

void Solution::generateSolution(int control1, int control2)
{
    partitionProcedure(control1, control2);
    createPaths();
}

void Solution::createPaths()
{
    z = 0;
    for (int i = 0; i < routes.size(); i++)
    {
        routes[i].generateRoute();
        z += routes[i].z;
    }
}

void Solution::partitionProcedure(int control1, int control2)
{
    using namespace std;

    // D -> total demand across all node
    int total_demand = accumulate(demands->begin(), demands->end(), 0);

    // K -> minimum number of routes
    int K = ceil(double(total_demand)/vehicle_capacity);

    // S -> total spare capacity across routes
    int sparing_routes = 0;
    for (int i = 0; i < routes.size(); i++)
    {
        sparing_routes += routes[i].spare_capacity;
    }
    int spare_capacity = (m_routes-routes.size())*vehicle_capacity - total_demand + sparing_routes;

    // A -> average spare capacity across routes
    int avg_spare_capacity = ceil(double(spare_capacity)/m_routes);

    // R -> nodes with unsatisfied demand
    vector<int> unsatisfied;

    int i, j, k;

    bool stop = false;
    if (m_routes >= K)
    {
        // selecting nodes with unsatisfied demand
        for (i = 1; i < n_nodes; i++)
            if (current_demand[i] > 0)
            {
                unsatisfied.push_back(i);
            }

        while (!stop)
        {
            if (routes.size() == m_routes)
                break; // no routes remaining

            // adding new route
            Route k_route(n_nodes, vehicle_capacity, costs);
            k = routes.size()+1;

            // selecting first node for route
            i = selectFirstNode(&unsatisfied, control1);
            k_route.visit(i);
			
			// new route can't fulfill selected node's demand
            if (current_demand[i] > k_route.spare_capacity)
            {
                k_route.deliver(i, k_route.spare_capacity);
                current_demand[i] -= k_route.delivered[i];
                avg_spare_capacity = updateAvgSpareCapacity(spare_capacity, m_routes, k, control2);
                routes.push_back(k_route);
            }
			
			// new route can fulfill selected node's demand but fail capacity threshold control
            else if (k_route.spare_capacity - current_demand[i] <= avg_spare_capacity && control2 != WHOLE)
            {
                unsatisfied.erase(remove(unsatisfied.begin(), unsatisfied.end(),
                                    i), unsatisfied.end());
                k_route.deliver(i, current_demand[i]);
                spare_capacity -= (k_route.spare_capacity + current_demand[i]);
                avg_spare_capacity = updateAvgSpareCapacity(spare_capacity, m_routes, k, control2);
                current_demand[i] = 0;
                routes.push_back(k_route);
            }
            else
            {
                unsatisfied.erase(remove(unsatisfied.begin(), unsatisfied.end(),
                                    i), unsatisfied.end());
                k_route.deliver(i, current_demand[i]);
                current_demand[i] = 0;

				// while there are nodes to be attended
                while (unsatisfied.size() >= 0)
                {
                    if (unsatisfied.size() == 0)
                    {
                        routes.push_back(k_route);
                        stop = true;
                        break;
                    }
					// only 1 node left
                    else if (unsatisfied.size() == 1)
                    {
                        i = unsatisfied[0];
                        k_route.visit(i);
						
						// can fulfill demand
                        if (current_demand[i] <= k_route.spare_capacity)
                        {
                            unsatisfied.erase(remove(unsatisfied.begin(), unsatisfied.end(),
                                    i), unsatisfied.end());
                            k_route.deliver(i, current_demand[i]);
                            current_demand[i] = 0;

                            routes.push_back(k_route);
                            stop = true;
                            break;
                        }
                        else
                        {
                            current_demand[i] -= k_route.spare_capacity;
                            k_route.deliver(i, k_route.spare_capacity);
                            avg_spare_capacity = updateAvgSpareCapacity(spare_capacity, m_routes, k, control2);

                            routes.push_back(k_route);
                            break;
                        }
                    }
					// more than 1 node left
                    else
                    {
                        double distance_from_route[unsatisfied.size()];
                        for (int x = 0; x < unsatisfied.size(); x++)
                        {
                            distance_from_route[x] = 0;
                            for (int y = 0; y < k_route.visited.size(); y++)
                                    if (k_route.visited[y] == 1)
                                        distance_from_route[x] += costs[unsatisfied[x]][y];
                        }
                        int closest = 0, second_closest = 1;
                        for (i = 0; i < unsatisfied.size(); i++)
                        {
                            if (distance_from_route[i] < distance_from_route[closest])
                            {
                                second_closest = closest;
                                closest = i;
                            }
                            else if (distance_from_route[i] < distance_from_route[second_closest])
                              if (i != closest) second_closest = i;
                        }

                        i = unsatisfied[closest];
                        j = unsatisfied[second_closest];

                        // route can't fulfill i's demand but can j's
                        if (current_demand[i] > k_route.spare_capacity
                            && current_demand[j] <= k_route.spare_capacity)
                        {
                            unsatisfied.erase(remove(unsatisfied.begin(), unsatisfied.end(),
                                    j), unsatisfied.end());
                            k_route.visit(j);
                            k_route.deliver(j, current_demand[j]);
                            current_demand[j] = 0;
							
							// capacity threshold control
                            if (k_route.spare_capacity <= avg_spare_capacity && control2 != WHOLE)
                            {
                                spare_capacity -= k_route.spare_capacity;
                                avg_spare_capacity = updateAvgSpareCapacity(spare_capacity, m_routes, k, control2);

                                routes.push_back(k_route);
                                break; // next route
                            }
                        }
						
						// route can fulfill i's demand but can't j's
                        else if (current_demand[i] <= k_route.spare_capacity
                            && current_demand[j] > k_route.spare_capacity)
                        {
                            unsatisfied.erase(remove(unsatisfied.begin(), unsatisfied.end(),
                                    i), unsatisfied.end());
                            k_route.visit(i);
                            k_route.deliver(i, current_demand[i]);
                            current_demand[i] = 0;
							
							// capacity threshold control
                            if (k_route.spare_capacity <= avg_spare_capacity && control2 != WHOLE)
                            {
                                spare_capacity -= k_route.spare_capacity;
                                avg_spare_capacity = updateAvgSpareCapacity(spare_capacity, m_routes, k, control2);

                                routes.push_back(k_route);
                                break; // next route
                            }
                        }
						
						// route can fulfill either i or j, but not both
                        else if (current_demand[i] <= k_route.spare_capacity
                            && current_demand[j] <= k_route.spare_capacity
                            && current_demand[i]+current_demand[j] > k_route.spare_capacity)
                        {
                            unsatisfied.erase(remove(unsatisfied.begin(), unsatisfied.end(),
                                    i), unsatisfied.end());
                            k_route.visit(i);
                            k_route.deliver(i, current_demand[i]);
                            current_demand[i] = 0;

                            if (k_route.spare_capacity == 0)
                            {
                                routes.push_back(k_route);
                                break; // next route
                            }
                            else
                            {
                                k_route.visit(j);
                                current_demand[j] -= k_route.spare_capacity;
                                k_route.deliver(j, k_route.spare_capacity);

                                routes.push_back(k_route);
                                break; // next route
                            }
                        }
						
						// route can fulfill both demands
                        else if (current_demand[i] <= k_route.spare_capacity
                            && current_demand[j] <= k_route.spare_capacity
                            && current_demand[i]+current_demand[j] <= k_route.spare_capacity)
                        {
                            unsatisfied.erase(remove(unsatisfied.begin(), unsatisfied.end(),
                                    i), unsatisfied.end());
                            k_route.visit(i);
                            k_route.deliver(i, current_demand[i]);
                            current_demand[i] = 0;

                            unsatisfied.erase(remove(unsatisfied.begin(), unsatisfied.end(),
                                    j), unsatisfied.end());
                            k_route.visit(j);
                            k_route.deliver(j, current_demand[j]);
                            current_demand[j] = 0;

                            if (k_route.spare_capacity <= avg_spare_capacity && control2 != WHOLE)
                            {
                                spare_capacity -= k_route.spare_capacity;
                                avg_spare_capacity = updateAvgSpareCapacity(spare_capacity, m_routes, k, control2);

                                routes.push_back(k_route);
                                break; // next route
                            }
                        }
						
						// route can't fulfill either demands
                        else if (current_demand[i] > k_route.spare_capacity
                            && current_demand[j] > k_route.spare_capacity)
                        {
                            k_route.visit(i);
                            current_demand[i] -= k_route.spare_capacity;
                            k_route.deliver(i, k_route.spare_capacity);
                            avg_spare_capacity = updateAvgSpareCapacity(spare_capacity, m_routes, k, control2);

                            routes.push_back(k_route);
                            break; // next route
                        }
                    }
                }
            }

        }
    }
    else
    {
        cout << "Infeasible." << endl;
    }
}

int Solution::selectFirstNode(std::vector<int>* R, int control)
{
    int selected = 0;
    switch (control)
    {
    // furthest node
    case FURTHEST:
        {
            for (unsigned int i = 0; i < R->size(); i++)
            {
                if (costs[0][R->at(i)] > costs[0][selected])
                    selected = R->at(i);
            }
            return selected;
        }

    // node with largest unsatisfied demand
    case LARGEST_DEMAND:
        {
            for (int i = 1; i < n_nodes; i++)
            {
                if (current_demand[i] > current_demand[selected])
                    selected = i;
            }
            return selected;
        }

    // node with smallest unsatisfied demand
    case SMALLEST_DEMAND:
        {
            for (int i = 1; i < n_nodes; i++)
            {
                if ((current_demand[i] < current_demand[selected] && current_demand[i] != 0) || current_demand[selected] == 0)
                    selected = i;
            }
            return selected;
        }

    // random node
    case RANDOM:
        {
            selected = rand()%R->size();
            return R->at(selected);
        }

    // furthest node on smallest arc
    case FURTHEST_SMALLEST_ARCH:
        {
            int arch_i, arch_j, arch_cost = 32768;
            for (int i = 0; i < R->size(); i++)
            {
                for (int j = i; j < R->size(); j++)
                    if (costs[R->at(i)][R->at(i)] < arch_cost)
                    {
                        arch_i = R->at(i);
                        arch_j = R->at(j);
                        arch_cost = costs[i][j];
                    }
            }
            if (costs[0][arch_i] > costs[0][arch_j])
                return arch_i;
            else return arch_j;
        }

    case RANDOM_EACH_ROUTE:
        return selectFirstNode(R, rand()%5);

    default:
        {
            selected = rand()%R->size();
            return R->at(selected);
        }
    }
}

int Solution::updateAvgSpareCapacity(int S, int m, int k, int control)
{
    switch (control)
    {
    case CEIL:
        return ceil(double(S)/(m-k));
    case FLOOR:
        return floor(double(S)/(m-k));
    default:
        return ceil(double(S)/(m-k));
    }
}

bool Solution::checkFeasibility(bool verbose)
{
    using namespace std;
    bool feasible = true;

    for (int k = 0; k < routes.size(); k++)
    {
        Route route = routes[k];
        int sum = 0;
        for (int i = 0; i < n_nodes; i++)
        {
            sum += route.delivered[i];
        }
        if (sum > vehicle_capacity)
        {
            feasible = false;
            if (verbose) cout << "Route " << k << " delivered " << sum << " (capacity " << vehicle_capacity << ")" << endl;
        }
    }

    for (int i = 0; i < n_nodes; i++)
    {
        int sum = 0;
        for (int k = 0; k < routes.size(); k++)
        {
            sum += routes[k].delivered[i];
        }
        if (sum != demands->at(i))
        {
            feasible = false;
            if (verbose) cout << "Node " << i << " received " << sum << " (demanded " << demands->at(i) << ")" << endl;
        }
    }
    return feasible;
}

void Solution::addRoute(Route r)
{
    for (int i = 0; i < n_nodes; i++)
        if (r.delivered[i] == 0) r.visited[i] = 0;
        else if (r.visited[i] == 0) r.delivered[i] = 0;

    for (int i = 0; i < n_nodes; i++)
    {
        current_demand[i] -= r.delivered[i];
    }
    routes.push_back(r);
}

void Solution::editRoute(int k)
{
    z -= routes[k].z;
    routes[k].opt3();
    z += routes[k].z;
}

Solution::~Solution()
{
    //dtor
}
