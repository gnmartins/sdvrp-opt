#include "../include/auxiliar.h"
#include "../include/filereader.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>

SDVRP readInstance(char *filename)
{
    using namespace std;
    FILE *file = fopen(filename, "r");
    if (file != NULL)
    {
        SDVRP instance;
        cout << "Reading instance " << filename << endl;
        /* preparing instance */
        {
            char buffer[255];

            fscanf(file, "%s : %s\n", buffer, instance.name);           //"NAME"
            fgets(buffer, 255, file);                                   //"COMMENT"
            fgets(buffer, 255, file);                                   //"TYPE"
            fscanf(file, "%s : %d\n", buffer, &instance.dimension);     //"DIMENSION"
            fgets(buffer, 255, file);                                   //"EDGE_WEIGHT_TYPE"
            fscanf(file, "%s : %d\n", buffer, &instance.capacity);      //"CAPACITY"

            fgets(buffer, 255, file);                                   //"NODE_COORD_SECTION"

            /* node_coord_section */
            Coord node;
            int aux;
            for (int i = 1; i <= instance.dimension; i++)
            {
                fscanf(file, "%d %d %d\n", &aux, &node.x, &node.y);
                instance.nodes.push_back(node);
            }

            fgets(buffer, 255, file);                                   //"DEMAND_SECTION"

            /* demand_section */
            int demand;
            for (int i = 1; i <= instance.dimension; i++)
            {
                fscanf(file, "%d %d\n", &aux, &demand);
                instance.demands.push_back(demand);
            }

            /* calculating edges cost */
            instance.setEdges();
        }
        return instance;
    }
    else cout << "File " << filename << " not found" << endl;
    return *(new SDVRP);
}
