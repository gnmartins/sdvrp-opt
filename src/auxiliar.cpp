#include "../include/auxiliar.h"
#include <cstdlib>
#include <ctime>
#include <math.h>
#include <iostream>
#include <algorithm>

/* ------ RANDOM ------*/

float random(float low, float high)
{
    float r = low + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(high-low)));
    r = low + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(high-low)));
    return r;
}

/* ------ SDVRP routes TSP ------ */

TSPSolution tsp(double** costs, std::vector<int> nodes)
{
    using namespace std;
    unsigned int i, nn, node;

    TSPSolution solution;
    solution.z = 0;
    int current = 0;
    solution.route.push_back(0);

    while (nodes.size() > 0)
    {
        nn = nodes[0];
        for (i = 0; i < nodes.size(); i++)
        {
            node = nodes[i];
            if (costs[current][node] < costs[current][nn] || nn == current)
                nn = node;
        }
        solution.z += costs[current][nn];
        solution.route.push_back(nn);
        nodes.erase(remove(nodes.begin(), nodes.end(), nn), nodes.end());
        current = nn;
    }
    solution.z += costs[current][0];
    solution.route.push_back(0);

    return solution;
}


/* ------- SDVRP PARTITION ------ */

///* CONTROL1
// 0-3
int firstNodeSelection(std::vector<int>* R, double** c, int *f, int n, int type)
{
    int selected = 0;
    switch (type)
    {
    case 0:
        for (unsigned int i = 0; i < R->size(); i++)
        {
            if (c[0][R->at(i)] > c[0][selected])
                selected = R->at(i);
        }
        return selected;

    case 1:
        for (int i = 1; i < n; i++)
        {
            if (f[i] > f[selected])
                selected = i;
        }
        return selected;

    case 2:
        for (int i = 1; i < n; i++)
        {
            if ((f[i] < f[selected] && f[i] != 0) || f[selected] == 0)
                selected = i;
        }
        return selected;

    case 3:
        selected = rand()%R->size();
        return R->at(selected);

    default:
        for (unsigned int i = 0; i < R->size(); i++)
        {
            if (c[0][R->at(i)] > c[0][selected])
                selected = R->at(i);
        }
        return selected;
    }

}

///*CONTROL2
// 0-1
int updateA(int S, int m, int k, int type)
{
    switch (type)
    {
    case 0:
        //round up
        return ceil(double(S)/(m-k));
    case 1:
        //round down
        return floor(double(S)/(m-k));
    default:
        return ceil(double(S)/(m-k));
    }
}
