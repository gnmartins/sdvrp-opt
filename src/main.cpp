#define FILENAME_SIZE 255

#include "../include/filereader.h"
#include "../include/SDVRP.h"
#include "../include/Solution.h"
#include "../include/Genetic.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctime>
#include <iostream>

int main(int argc, char *argv[])
{
    using namespace std;
    srand(time(0));

    cout << "----------------------------------------------" << endl;
    cout << "Split Delivery Vehicle Routing Problem (SDVRP)" << endl;
    cout << "----------------------------------------------" << endl;
    if (argc == 3)
    {
        char filename[FILENAME_SIZE];
        strcpy(filename, argv[1]);

        SDVRP instance = readInstance(filename);
        char *p;
        instance.max_vehicles = strtol(argv[2], &p, 10);

        instance.printInstance();

        for (int i = 0; i < 10; i++)
        {
            Genetic algorithm(100, 50, 1.0, 0.1, instance);
            Solution x = algorithm.run();
            x.checkFeasibility(true);
            x.printSolution();
        }

    }
    else
    {
        cout << "Please enter instance file (*.sd) and maximum number of vehicles" << endl;
        cout << "\t./sdvrp instance.sd <max_number_of_vehicles>" << endl;
    }
}
