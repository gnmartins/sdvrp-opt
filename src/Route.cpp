#include "Route.h"

#include <algorithm>
#include <iostream>

Route::Route(int nodes, int capacity, double** costs)
{
    this->spare_capacity = capacity;
    this->costs = costs;
    this->nodes = nodes;

    this->z = 0;

    std::vector<int> temp(nodes, 0);
    this->visited = temp;
    this->delivered = temp;
    for (int i = 0; i < nodes; i++)
    {
        this->visited[i] = 0;
        this->delivered[i] = 0;
    }
}

void Route::generateRoute()
{
    using namespace std;
    path.clear();

    for (int i = 0; i < nodes; i++)
    {
        if (visited[i] == 1 && delivered[i] == 0) visited[i] == 0;
    }

    vector<int> nodes_visited;
    for (int i = 0; i < nodes; i++)
        if (visited[i] == 1)
            nodes_visited.push_back(i);

    int current = 0, nn, node;
    path.push_back(0);
    z = 0;

    while (nodes_visited.size() > 0)
    {
        nn = nodes_visited[0];
        for (int i = 0; i < nodes_visited.size(); i++)
        {
            node = nodes_visited[i];
            if (costs[current][node] < costs[current][nn] || nn == current)
                nn = node;
        }
        z += costs[current][nn];
        path.push_back(nn);
        nodes_visited.erase(remove(nodes_visited.begin(), nodes_visited.end(), nn), nodes_visited.end());
        current = nn;
    }
    z += costs[current][0];
    path.push_back(0);
}

void Route::visit(int node)
{
    this->visited[node] = 1;
}

void Route::deliver(int node, int quantity)
{
    this->delivered[node] = quantity;
    this->spare_capacity -= quantity;
}

void Route::printRoute()
{
    using namespace std;
    for (unsigned int j = 0; j < path.size(); j++)
        if (j == path.size()-1)
            cout << path[j] << "(" << delivered[path[j]] << ")";
        else
            cout << path[j] << "(" << delivered[path[j]] << ") -> ";
    cout << "\n    cost = " << z << endl;
}

bool Route::isConflicting(Route other)
{
    for (int i = 0; i < visited.size(); i++)
        if (visited[i] == 1 && other.visited[i] == 1)
            return true;
    return false;
}

void Route::opt3()
{
    int x = rand()%(path.size()-2);
    x += 1;
    int y = rand()%(path.size()-2);
    y += 1;
    int p = rand()%(path.size()-2);
    p += 1;

    int old_x = path[x];
    int old_y = path[y];
    int old_p = path[p];

    path[x] = old_y;
    path[y] = old_p;
    path[p] = old_x;



    z = 0;
    for (int i = 0; i < path.size()-1; i++)
        z += costs[path[i]][path[i+1]];
}

Route::~Route()
{
    //dtor
}
