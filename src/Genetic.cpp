#include "Genetic.h"

#include <algorithm>
#include <numeric>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <time.h>

/* as seen on http://stackoverflow.com/a/686373 */
float random_float(float LO, float HI)
{
    return LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
}

bool solution_sort(Solution &s1, Solution &s2)
{
    return s1.z < s2.z;
}

bool route_sort(Route& r1, Route& r2)
{
    int total_r1 = std::accumulate(r1.visited.begin(), r1.visited.end(), 0);
    int total_r2 = std::accumulate(r2.visited.begin(), r2.visited.end(), 0);
    return r1.z/total_r1 < r2.z/total_r2;
}

Genetic::Genetic(int population_size, int max_generations, float crossover_rate, float mutation_rate, SDVRP instance)
{
    this->population_size = population_size;
    this->max_generations = max_generations;
    this->crossover_rate = crossover_rate;
    this->mutation_rate = mutation_rate;
    this->instance = instance;
}

void Genetic::firstGeneration()
{
    for (int i = 0; i < 5 && population.size() < population_size; i++)
        for (int j = 0; j < 3 && population.size() < population_size; j++)
        {
            Solution solution(instance.max_vehicles,
                              instance.dimension,
                              instance.capacity,
                              instance.getEdges(),
                              &instance.demands);
            solution.generateSolution(i, j);
            population.push_back(solution);
        }

    while (population.size() < population_size)
    {
        Solution solution(instance.max_vehicles,
                              instance.dimension,
                              instance.capacity,
                              instance.getEdges(),
                              &instance.demands);
            solution.generateSolution(5, rand()%3);
            population.push_back(solution);
    }
}

void Genetic::printPopulation()
{
    for (int i = 0; i < population.size(); i++)
    {
        population[i].printSolution();
    }
}

Solution Genetic::run()
{
    using namespace std;
    firstGeneration();
    //printPopulation();

    clock_t tStart = clock();

    float best_0;
    float avg_0;
    float best_n;
    float avg_n;

    sort(population.begin(), population.end(), solution_sort);
    double sum = 0;
    for (int i = 0; i < population.size(); i++)
        sum += population[i].z;

    best_0 = population[0].z;
    avg_0 = sum/population.size();
    cout << "Generation " << 0 << " best fitness: " << best_0 << endl;
    cout << "    Average fitness: " << avg_0 << endl;

    int gen = 1;
    while (gen < max_generations + 1)
    {
        cout << "Gen " << gen << endl;
        /* selecting parents to produce offspring
            - best third from previous generation
            - random individuals so parents size is equal to half of previous generation size */
        sort(population.begin(), population.end(), solution_sort);
        vector<Solution> new_population(population.begin(), population.begin()+2);

        vector<Solution> parents;
        while (parents.size() < population_size/2)
        {
            int x = selection();
            parents.push_back(population[selection()]);
            population.erase(population.begin()+x);
        }

        /* generating offspring */
        int father, mother;
        while (new_population.size() < population_size)
        {
            father = rand()%parents.size();
            mother = rand()%parents.size();
            while (mother == father)
                mother = rand()%parents.size();

            Solution child = parents[father];
            if (random_float(0, 1) <= crossover_rate)
            {
                child = crossover(parents[father], parents[mother]);
            }
            if (random_float(0, 1) <= mutation_rate)
            {
                int r = rand()%child.routes.size();
                child.editRoute(r);
            }

            new_population.push_back(child);
        }
        population = new_population;

        gen++;
    }

    sort(population.begin(), population.end(), solution_sort);
    sum = 0;
    for (int i = 0; i < population.size(); i++)
        sum += population[i].z;

    best_n = population[0].z;
    avg_n = sum/population.size();
    cout << "Generation " << 0 << " best fitness: " << best_n << endl;
    cout << "    Average fitness: " << avg_n << endl;

    double time = (double) (clock() - tStart)/CLOCKS_PER_SEC;

    ofstream outfile;
    outfile.open(instance.name, ios_base::app);
    outfile << best_0 << " " << avg_0 << " " << best_n << " " << avg_n << " " << time << endl;

    return population[0];
}

int Genetic::selection()
{
    using namespace std;

    float sp = 1.5; //selective pressure

    sort(population.begin(), population.end(), solution_sort);

    vector<float> ranks;
    float _rank;
    for (int i = 0; i < population.size(); i++)
    {
        _rank = 2 - sp + (2 * (sp-1) * (i)/(population.size()-1));
        ranks.push_back(_rank);
    }
    reverse(ranks.begin(), ranks.end());

    float sum = accumulate(ranks.begin(), ranks.end(), 0);

    float selected = random_float(0, sum);
    float acc = ranks[0];
    int i;
    for (i = 0; i < ranks.size(); i++)
    {
        if (acc <= selected) acc += ranks[i];
        else break;
    }

    return i;
}

Solution Genetic::crossover(Solution father, Solution mother)
{

    using namespace std;
    Solution child(instance.max_vehicles,
                   instance.dimension,
                   instance.capacity,
                   instance.getEdges(),
                   &instance.demands);

    vector<Route> father_routes;
    for (int i = 0; i < father.routes.size(); i++)
        father_routes.push_back(father.routes[i]);
    sort(father_routes.begin(), father_routes.end(), route_sort);

    vector<Route> mother_routes;
    for (int i = 0; i < mother.routes.size(); i++)
        mother_routes.push_back(mother.routes[i]);
    sort(mother_routes.begin(), mother_routes.end(), route_sort);

    Route helper = mother_routes[0];
    if (father_routes[0].z > mother_routes[0].z)
        Route helper = father_routes[0];

    int s = 0;
    while (father_routes.size() > 0 && mother_routes.size() > 0)
    {
        Route selected = father_routes[0];

        /* deleting conflicting routes */
        if (s%2 == 0)
        {
            selected = father_routes[0];
            father_routes.erase(father_routes.begin());

            for (int i = mother_routes.size()-1; i >= 0; i--)
                if (selected.isConflicting(mother_routes[i]))
                    mother_routes.erase(mother_routes.begin()+i);
        }
        else
        {
            selected = mother_routes[0];
            mother_routes.erase(mother_routes.begin());

            for (int i = father_routes.size()-1; i >= 0; i--)
                if (selected.isConflicting(father_routes[i]))
                    father_routes.erase(father_routes.begin()+i);
        }

        child.addRoute(selected);
        s++;
    }

    child.generateSolution(5, rand()%3);
    if (!child.checkFeasibility(false))
    {
        Solution child2(instance.max_vehicles,
                   instance.dimension,
                   instance.capacity,
                   instance.getEdges(),
                   &instance.demands);
        child2.addRoute(helper);
        child2.generateSolution(5, rand()%3);
        return child2;
    }

    return child;
}

Genetic::~Genetic()
{
    //dtor
}
