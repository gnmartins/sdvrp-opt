#include "../include/SDVRP.h"
#include <math.h>
#include <iostream>

SDVRP::SDVRP()
{
}

SDVRP::~SDVRP()
{
}

void SDVRP::setEdges()
{
    edges = new double*[dimension];
    for (int i = 0; i < dimension; i++)
        edges[i] = new double[dimension];

    double x, y, dist;
    for (int i = 0; i < dimension; i++)
    {
        for (int j = i; j < dimension; j++)
        {
            x = nodes[i].x - nodes[j].x;
            y = nodes[i].y - nodes[j].y;
            dist = pow(x, 2) + pow(y, 2);
            dist = sqrt(dist);
            edges[i][j] = dist;
            edges[j][i] = dist;
        }
    }
}

double** SDVRP::getEdges()
{
    return edges;
}

void SDVRP::printInstance()
{
    using namespace std;
    cout << "----------------------------------------" << endl;
    cout << "NAME: " << name << endl;
    cout << "DIMENSION: " << dimension << endl;
    cout << "MAXIMUM NUMBER OF VEHICLES: " << max_vehicles << endl;
    cout << "NODE \tX\tY\tDEMAND" << endl;
    for (int i = 0; i < dimension; i++)
    {
        cout << i << "\t" << nodes[i].x << "\t" << nodes[i].y << "\t";
        cout << demands[i] << endl;
    }
    /*
    for (int i = 0; i < dimension; i++)
    {
        for (int j = 0; j < dimension; j++)
            cout << edges[i][j] << " ";
        cout << endl;
    }
    /**/
    cout << "----------------------------------------" << endl;
}
