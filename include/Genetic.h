#ifndef GENETIC_H
#define GENETIC_H

#include <vector>
#include "../include/Solution.h"
#include "../include/SDVRP.h"

class Genetic
{
    public:
        Genetic(int population_size, int max_generations, float crossover_rate, float mutation_rate, SDVRP instance);
        virtual ~Genetic();

        void firstGeneration();
        Solution run();

        int selection();
        Solution crossover(Solution father, Solution mother);

        void printPopulation();

    protected:

    private:
        int population_size;
        int max_generations;
        float crossover_rate;
        float mutation_rate;

        std::vector<Solution> population;
        SDVRP instance;
};

#endif // GENETIC_H
