#ifndef AUXILIAR_H_INCLUDED
#define AUXILIAR_H_INCLUDED

#include <vector>

typedef struct _coord
{
    int x;
    int y;
} Coord;

typedef struct _tspsol
{
    std::vector<int> route;
    double z;
} TSPSolution;

float random(float low, float high);
int updateA(int S, int m, int k, int type);
int firstNodeSelection(std::vector<int>* R, double** c, int* f, int n, int type);
TSPSolution tsp(double** costs, std::vector<int> nodes);



#endif // AUXILIAR_H_INCLUDED
