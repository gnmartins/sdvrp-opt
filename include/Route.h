#ifndef ROUTE_H
#define ROUTE_H

#include <vector>

class Route
{
    public:
        Route(int nodes, int capacity, double** costs);
        virtual ~Route();
        void visit(int node);
        void deliver(int node, int quantity);
        void generateRoute();
        void printRoute();

        bool isConflicting(Route other);
        void opt3();

        int spare_capacity;
        std::vector<int> visited;
        std::vector<int> delivered;
        std::vector<int> path;
        double z;

    protected:

    private:
        double** costs;
        int nodes;


};

#endif // ROUTE_H
