#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>
#include "../include/Route.h"


class Solution
{
    public:
        Solution(int routes, int nodes, int capacity, double** edges, std::vector<int>* demands);
        virtual ~Solution();

        void generateSolution(int control1, int control2);
        void printSolution();

        bool checkFeasibility(bool verbose);
        void addRoute(Route r);
        void editRoute(int k);

        void createPaths();

        void partitionProcedure(int control1, int control2);
        int selectFirstNode(std::vector<int>* R, int control);
        int updateAvgSpareCapacity(int S, int m, int k, int control);

        std::vector<Route> routes;
        std::vector<int> current_demand;
        double z;

        // control1
        enum
        {
            FURTHEST,
            LARGEST_DEMAND,
            SMALLEST_DEMAND,
            RANDOM,
            FURTHEST_SMALLEST_ARCH,
            RANDOM_EACH_ROUTE
        };

        // control2
        enum
        {
            CEIL,
            FLOOR,
            WHOLE
        };


    protected:

    private:
        int m_routes;
        int n_nodes;
        int vehicle_capacity;
        double** costs;
        std::vector<int>* demands;


};

#endif // SOLUTION_H
