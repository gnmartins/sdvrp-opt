#ifndef SDVRP_H
#define SDVRP_H

#include "../include/auxiliar.h"
#include <vector>
#define MAX_NAME 255


class SDVRP
{
    public:
        SDVRP();
        virtual ~SDVRP();
        void setEdges();
        void printInstance();
        double** getEdges();

        char name[MAX_NAME];
        int max_vehicles;
        int dimension;
        int capacity;
        std::vector<Coord> nodes;
        std::vector<int> demands;
        double** edges;

        float fitness;

    protected:

    private:

};

#endif // SDVRP_H
